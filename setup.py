#!/usr/bin/env python
# -*- coding: utf-8 -*-

# System imports
from distutils.core import setup

from os.path import join as pjoin
import glob
import platform
import sys

setup(name = "units",
      description = """
      A python package for handling of physical units.
      Originally from the larger software package of pyopennm.
      """,
      author = "Christopher M. Bruns",
      author_email = "cmbruns@stanford.edu",
      packages = ["units"],
      license = "MIT",
      )
